#遍历指定目录下的所有OC文件内容

import os
import re
import xmltodict

path = "/Users/a33/Desktop/CloudMinerbackup/CloudMiner/CloudMiner/CMHoldViewController"

# 读取目录下的所有文件
def travelFile(path):
    result = []
    for root, dirs, files in os.walk(path):
        if len(files) > 0:
            for item in files:
                
                hasCMPrefix = item.startswith('CM') or item.startswith('FZM')
                if hasCMPrefix == False: # 文件名必须是CM 或者FZM开头
                    continue

                vaild = (item.endswith('.h') or item.endswith('.m'))
                if vaild:
                    filePath = root + "/" + item
                    result.append(filePath)
                

    return result


# result = travelFile(path)

# 读取oc文件
filePath = "/Users/a33/Desktop/CloudMinerbackup/CloudMiner/CloudMiner/AppDelegate.m"
def travelLines(filePath, xmlList):
    w_str = ""
    notFindStr = ""
    with open(filePath, "r") as f:
        line = f.readline()
        while line:
            re_words = re.compile(u"@\"[\u4e00-\u9fa5]+\"")
            m =  re_words.search(line,0)
            if m == None:
                w_str += line
                line = f.readline()
                continue

            #找到中文字符, 去匹配
            march = marchKey(m.group(), xmlList)
            if march == False:
                #不在xml文件里
                # print("m.grout 不在xml中" + m.group())
                w_str += line
                notFindStr += m.group() + "\n"
                line = f.readline()
                continue
            
            ocFormat = "NSLocalizedStringFromTable(@\"" + march + "\",nil,nil)"
            r = line.replace(m.group(), ocFormat)
            # print("替换后" + r)
            w_str += r
            line = f.readline()
    return (w_str, notFindStr)

            
            

# travelLines(filePath)
  
# 读取xml
xmlPath = "/Users/a33/Desktop/cloudString/strings.xml"
def readXML(path):
    ocString = {}
    with open(path) as fd:

        obj = xmltodict.parse(fd.read())
        resources =  obj["resources"]
        strings = resources["string"]
        nameKey = "@name"
        valueKey = "#text"
        for item in strings:
            value = ""
            key = ""
            for k, v in item.items():
                if k == nameKey:
                    key = v
                if k == valueKey:
                    value = "@\"" + v + "\""
                    
            ocString[value] = key
               
    return ocString


# 判断字符是否在xml文件里
def marchKey(key, xml):
    # print("key" + key)
    if key in xml:
        return xml[key]
    return False
        
# 将替换后的文件写回oc文件
def writeToFile(path, str):
    with open(path, "w") as f:
        f.write(str)


notFindPath = "/Users/a33/Desktop/cloudString/notFind.txt"
def writeNotFindStr(writePath, ocPath, str):
    nStr = "\n-----------\n" + ocPath + "\n" + str
    with open(writePath , "a") as f:
        f.write(nStr)
    

# ----- 先把xml里的键值对读出来
xmlList = readXML(xmlPath)

# --------- 遍历oc目录, 找到所有CM, FZM 开头的文件
ocRootPath = "/Users/a33/Desktop/CloudMinerbackup/CloudMiner/CloudMiner"
filePaths = travelFile(ocRootPath)

# ------- 开始替换
for filePath in filePaths:
    # --- 替换
    travelResult = travelLines(filePath, xmlList)
    #--- 替换后的字符串写回源文件
    w_str = travelResult[0]
    writeToFile(filePath, w_str)

    # --- 没有找到的文件 记录一下
    notFind_str = travelResult[1]
    writeNotFindStr(notFindPath,filePath,notFind_str)

      
