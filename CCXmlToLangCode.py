# 将安卓的字符文件 string.xml, 转换成oc 多语言的stirng 文件

import CCReadXMLToList
import CCWriteListToFile

directoryXML = "/Users/a33/Desktop/cloudString"  # xml路径
fileNameXML = ['strings', 'strings1','strings2','strings3','strings4'] # xml文件名
suffixXML = ".xml"

directoryOC = "/Users/a33/Desktop/CloudMinerbackup/CloudMiner/CloudMiner" #oc文件路径
fileNameOC = ["zh-Hans.lproj"]
suffixOC = "InfoPlist.strings"

pathOC =  directoryOC + "/" + fileNameOC[0] + "/" + suffixOC # oc的路径

for name in fileNameXML:
    path = directoryXML + "/" + name + suffixXML
    firstLine = "//"  + name + "\n\n\n"   # 安卓分模块了, oc我准备写在一个string 文件内部, 为了区分, 在每个安卓模块前加上了 文件名和三个换行
    CCWriteListToFile.writeFileWithList([firstLine], pathOC)
    # result = CCWriteListToFile.readXML(path)
    result = CCReadXMLToList.readXML(path)
    CCWriteListToFile.writeFileWithList(result, pathOC)