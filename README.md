# CCXmlToLangCode

#### 项目介绍
iOS多语言, 将安卓的字符文件 string.xml, 转换成oc 多语言的stirng 文件

#### 文件介绍

 以下三个文件组合使用可以将安卓的xml文件转换成oc国际化的格式, 写入ocstring文件
  
 说明 https://blog.csdn.net/Baby_come_here/article/details/82691532
> CCXmlToLangCode 组合调用
> 
> CCWriteListToFile  写入文件
> 
> CCReadXMLToList 将xml文件转出oc国际化格式,并以list形式返回



 CCTravelOCFile 这个文件的作用是 
> 
> 1 遍历指定的根目录, 获取所有的合法文件. (文件合法的条件是1. 以CM 或FZM开头, && 是.h or .m文件.) 
> 
> 2 逐行遍历合法文件, 找到合法的字符(1. 必须是@"中文" && 2. 必须在xml文件中能找到). 
> 
> 3 将合法字符使用 NSLocalizedStringFromTable(xml中的key,nil,nil); 替换
> 
> 4 写入oc文件
> 5 对是@"中文" && 在xml字符中没有找到的字符, 会写入到notFind.txt 文件中


#### 环境

环境 Python 3

#### 依赖
xmltodict : 是一个读取xml的第三方

官方 https://github.com/martinblech/xmltodict.git

使用 https://blog.csdn.net/Baby_come_here/article/details/82688020

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)